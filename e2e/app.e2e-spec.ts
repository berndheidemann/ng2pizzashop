import { PizzaShopPage } from './app.po';

describe('pizza-shop App', function() {
  let page: PizzaShopPage;

  beforeEach(() => {
    page = new PizzaShopPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
