import { AppComponent } from '../app.component';
import { ImpressumComponent } from '../components/impressum/impressum.component';
import { AnfahrtComponent } from '../components/anfahrt/anfahrt.component';
import { Routes } from '@angular/router';
import { ShopComponent } from '../components/shop/shop.component';
import { AddPizzaComponent } from '../components/add-pizza/add-pizza.component';

export const routerConfig: Routes = [
    {
        path: 'shop',
        component: ShopComponent
    },
     {
        path: 'addPizza',
        component: AddPizzaComponent
    },
    {
        path: 'impressum',
        component: ImpressumComponent
    },
    {
        path: 'anfahrt',
        component: AnfahrtComponent
    },
    {
        path: '',
        redirectTo: '/shop',
        pathMatch: 'full'
    },
    {
        path: '**',
        redirectTo: '/shop',
        pathMatch: 'full'
    }
];
