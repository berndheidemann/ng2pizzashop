import { Pizza } from './pizza';

export class PizzaCartItem {

    public count = 1;

    constructor(public pizza: Pizza, public cartId: number) {
    }

    compare(item: PizzaCartItem): number {
        if (this.cartId === item.cartId) {
            return 0;
        }
        if (this.cartId < item.cartId) {
            return -1;
        }else {
            return 1;
        }
    }

}
