import {Pizza} from './pizza';

describe('Pizza', () => {
  it('should create an instance', () => {
    expect(new Pizza(1, 'Salami', 4.99)).toBeTruthy();
  });

  it('should be created with the correct name and price', () => {
    const p: Pizza = new Pizza(1, 'Salami', 4.99);
    expect(p.id === 1 && p.name === 'Salami' && p.price === 4.99).toBeTruthy();
  });

});
