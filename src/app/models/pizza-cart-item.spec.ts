import { Pizza } from './pizza';
import { PizzaCartItem } from './pizza-cart-item';

describe('PizzaCartItem', () => {
  it('should create an instance', () => {
    expect(new PizzaCartItem(new Pizza(1, 'Salami', 1.99), 3)).toBeTruthy();
  });

  it('should compare each other by cartId', () => {
    const item1: PizzaCartItem = new PizzaCartItem(new Pizza(1, 'Salami', 1.99), 3);
    item1.cartId = 1;
    const item2: PizzaCartItem = new PizzaCartItem(new Pizza(2, 'Kobe Rind', 1.99), 3);
    item2.cartId = 2;
    const item3: PizzaCartItem = new PizzaCartItem(new Pizza(3, 'GoldLachs', 1.99), 3);
    item3.cartId = 3;
    const item4: PizzaCartItem = new PizzaCartItem(new Pizza(4, 'Kobe Rind', 1.99), 3);
    item4.cartId = 4;
    expect(item1.compare(item2)).toBe(-1);
    expect(item3.compare(item2)).toBe(1);
    expect(item3.compare(item3)).toBe(0);

  });

});
