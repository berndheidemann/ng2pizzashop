import { Pizza } from '../models/pizza';

import { Injectable } from '@angular/core';

@Injectable()
export class PizzaDataServiceService {

  private pizzaList: Pizza[] = [];

  constructor() {
    this.addPizza(new Pizza(1, 'Käse Rumpsteak Blattgold', 27.99));
    this.addPizza(new Pizza(2, 'Dry Aged Rinderfilet Tomate Mozarella', 16.99));
    this.addPizza(new Pizza(3, 'Thunfisch Kobe Rind', 37.99));
    this.addPizza(new Pizza(4, 'Dry Aged Pulled Pork', 12.99));
    this.addPizza(new Pizza(5, 'Flyer', 0));
   }

  addPizza(pizza: Pizza) {
    this.pizzaList.push(pizza);
  }

  getPizzaById(id: number): Pizza {
    return this.pizzaList.filter(p => p.id === id).pop();
  }

  get pizzas(): Pizza[] {
    return this.pizzaList;
  }

}
