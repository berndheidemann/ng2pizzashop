import { Pizza } from '../models/pizza';

import { Injectable } from '@angular/core';

@Injectable()
export class PizzaDataServiceMock {

    private pizzaList: Pizza[]=[];
        constructor() {
        const pizza1: Pizza = new Pizza(1, 'Käse Salami Kobe', 17.99);
        const pizza2: Pizza = new Pizza(2, 'Rinderfilet mit Blattgold', 33.99);
        const pizza3: Pizza = new Pizza(3, 'Flyer', 0);
        const pizza4: Pizza = new Pizza(4, 'Haifischflosse auf Oregano-Käse', 20.10);
        this.pizzaList.push(pizza1);
        this.pizzaList.push(pizza2);
        this.pizzaList.push(pizza3);
        this.pizzaList.push(pizza4);
    }

    get pizzas(): Pizza[] {
        return this.pizzaList;
    }

    getPizzaById(id: number): Pizza {
        return this.pizzaList.find(p => p.id === id);
    }

     addPizza(pizza: Pizza) {
         this.pizzas.push(pizza);
     }
}
