/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { Pizza } from '../models/pizza';
import { PizzaDataServiceMock } from './pizza-data-service-mock';
import { PizzaDataServiceService } from './pizza-data-service.service';

describe('PizzaDataServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PizzaDataServiceService]
    });
  });

  it('should ...', inject([PizzaDataServiceService], (service: PizzaDataServiceService) => {
    expect(service).toBeTruthy();
  }));

  it('should save a Pizza and return it by id', inject([PizzaDataServiceService], (service: PizzaDataServiceService) => {
    const pizza: Pizza = new Pizza(1, 'Käse Salami', 7.99);
    service.addPizza(pizza);
    expect(service.getPizzaById(1).name).toEqual('Käse Salami');
  }));

  it('should return all pizzas', inject([PizzaDataServiceService], (service: PizzaDataServiceService) => {
    const pizza1: Pizza = new Pizza(7, 'Käse Salami', 7.99);
    const pizza2: Pizza = new Pizza(8, 'Salami', 3.99);
    service.addPizza(pizza1);
    service.addPizza(pizza2);
    expect(service.getPizzaById(8).name).toEqual('Salami');
  }));

  it('Mock should return a Salami pizza', inject([PizzaDataServiceService], (service: PizzaDataServiceService) => {
    const mock: PizzaDataServiceMock = new PizzaDataServiceMock();
    expect(mock.getPizzaById(2).name).toEqual('Rinderfilet mit Blattgold');
  }));
});
