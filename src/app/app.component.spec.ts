/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { AppModule } from './app.module';
import { CartItemComponent } from './components/cart-item/cart-item.component';
import { PizzaCartComponent } from './components/pizza-cart/pizza-cart.component';
import { PriceComponent } from './components/price/price.component';
import { Pizza } from './models/pizza';
import { PizzaPipe } from './pipes/pizza.pipe';
import { PizzaDataServiceMock } from './services/pizza-data-service-mock';
import { PizzaDataServiceService } from './services/pizza-data-service.service';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';


describe('AppComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule, RouterTestingModule ],
      declarations: [
        AppComponent, PizzaPipe, PriceComponent, CartItemComponent, PizzaCartComponent
      ],
      providers: []
    });
    TestBed.overrideComponent(AppComponent, {
      set: {
        providers: [
          { provide: PizzaDataServiceService, useClass: PizzaDataServiceMock },
        ]
      }
    });
    TestBed.compileComponents();
  });

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
