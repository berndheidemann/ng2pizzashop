/* tslint:disable:no-unused-variable */
import { Pizza } from '../models/pizza';
import { TestBed, async } from '@angular/core/testing';
import { PizzaPipe } from './pizza.pipe';

describe('PizzaPipe', () => {
  it('create an instance', () => {
    const pipe = new PizzaPipe();
    expect(pipe).toBeTruthy();
  });

  it('filters PizzaList by Name', () => {
    const pipe = new PizzaPipe();
    const pizza1: Pizza = new Pizza(1, 'Käse Salami', 7.99);
    const pizza2: Pizza = new Pizza(2, 'Salami', 3.99);
    const pizzaList: Pizza[] = [];
    pizzaList.push(pizza1);
    pizzaList.push(pizza2);

    expect(pipe.transform(pizzaList, 'Käse')[0].id).toEqual(1);
  });

   it('filters PizzaList by Name nonCaseSesitive', () => {
    const pipe = new PizzaPipe();
    const pizza1: Pizza = new Pizza(1, 'Käse Salami', 7.99);
    const pizza2: Pizza = new Pizza(2, 'Salami', 3.99);
    const pizzaList: Pizza[] = [];
    pizzaList.push(pizza1);
    pizzaList.push(pizza2);

    expect(pipe.transform(pizzaList, 'käse')[0].id).toEqual(1);
  });
});
