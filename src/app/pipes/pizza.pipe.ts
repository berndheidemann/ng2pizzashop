import { Pipe, PipeTransform } from '@angular/core';
import { Pizza } from '../models/pizza';

@Pipe({
  name: 'pizzaPipe'
})
export class PizzaPipe implements PipeTransform {

  transform(value: Pizza[], filter: string): any {
    if (filter === undefined || filter.length === 0) {
      return value;
    }
    return value.filter(p => p.name.toLowerCase().indexOf(filter.toLowerCase()) >= 0);
  }
}
