import { Pizza } from './models/pizza';
import { PizzaCartItem } from './models/pizza-cart-item';
import { PizzaDataServiceService } from './services/pizza-data-service.service';
import { Component, Output, ViewChild, EventEmitter } from '@angular/core';
import { PriceComponent } from './components/price/price.component';
import { PizzaCartComponent } from './components/pizza-cart/pizza-cart.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  constructor() {
  }

}
