/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Pizza } from '../../models/pizza';
import { PizzaCartItem } from '../../models/pizza-cart-item';
import { CartItemComponent } from '../cart-item/cart-item.component';
import { PriceComponent } from '../price/price.component';
import { PizzaCartComponent } from './pizza-cart.component';
import { log } from 'util';

describe('PizzaCartComponent', () => {
  let component: PizzaCartComponent;
  let fixture: ComponentFixture<PizzaCartComponent>;
  const comp: PizzaCartComponent = null;
  // let timerCallback;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PizzaCartComponent, PriceComponent, CartItemComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    // timerCallback = jasmine.createSpy('timerCallback');
    // jasmine.clock().install();
    fixture = TestBed.createComponent(PizzaCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    const p1: Pizza = new Pizza(3, 'Salami', 12.99);
    const p2: Pizza = new Pizza(4, 'Salami Kobe Rind', 13.01);
    component.addPizza(p1);
    component.addPizza(p2);
  });

  //  afterEach(function() {
  //   jasmine.clock().uninstall();
  // });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add a Pizza to Cart', () => {
    const p: Pizza = new Pizza(3, 'Salami', 12.99);
    component.addPizza(p);
    expect(component.cartItems.filter(ci => ci.pizza.id === 3).pop().pizza.name).toBe('Salami');
  });

  it('should calculate the CartSum', () => {
    expect(component.cartsum).toBe(26);
    component.addPizza(new Pizza(5, 'Salami Kobe Schwein', 14.01));
    expect(component.cartsum).toBe(40.01);
    const p1: Pizza = new Pizza(3, 'Salami', 12.99);
    component.addPizza(p1);
    expect(component.cartsum).toBe(53);

  });

  it('should display CartSum', async(() => {
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#cartsum').textContent).toContain('26 €');
  }));


  it('should count similar Orders', async(() => {
    const p2: Pizza = new Pizza(4, 'Salami Kobe Rind', 13.01);
    component.addPizza(p2);
    expect(component.cartItems.find(ci => ci.pizza.id === 4).count).toBe(2);
  }));

  it('should display count similar Orders', async(() => {
    const p2: Pizza = new Pizza(4, 'Salami Kobe Rind', 13.01);
    component.addPizza(p2);
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#cart-2 .cart-item-count').textContent).toContain('2');
  }));

  it('should accept only one Flyer in Cart', async(() => {
    const p2: Pizza = new Pizza(5, 'Flyer', 0);
    const p3: Pizza = new Pizza(5, 'Flyer', 0);
    component.addPizza(p2);
    component.addPizza(p3);
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#cart-2 .cart-item-count').textContent).toContain('1');
  }));

  it('should display a warnMessage for multiple Flyer', async(() => {
    const p2: Pizza = new Pizza(5, 'Flyer', 0);
    const p3: Pizza = new Pizza(5, 'Flyer', 0);
    component.addPizza(p2);
    component.addPizza(p3);
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('.cartMessage').textContent).toContain('Nur ein Flyer pro Bestellung');
  }));


  it('should order the pizza-order alphabetical, but flyer to the end', async(() => {
    const p2: Pizza = new Pizza(5, 'Alami', 0);
    const p3: Pizza = new Pizza(3, 'BLami', 0);
    component.addPizza(p2);
    component.addPizza(p3);
    fixture.detectChanges();
    expect(component.cartItems[0].cartId < component.cartItems[1].cartId).toBeTruthy();
  }));

  it('should remove pizzas from cart', async(() => {
    component.removePizzaCartItem(1);
    expect(component.cartItems.length).toBe(1);
    expect(component.cartItems[0].pizza.name).toBe('Salami Kobe Rind');
  }));

  it('should remove pizzas from cart by click', async(() => {
    fixture.detectChanges();
    fixture.nativeElement.querySelector('#cart-1').dispatchEvent(new Event('click'));
    fixture.detectChanges();
    expect(component.cartItems.length).toBe(1);
    expect(component.cartItems[0].pizza.name).toBe('Salami Kobe Rind');
  }));

  it('should decrement pizzaCount from cart by click', async(() => {
    const p1: Pizza = new Pizza(3, 'Salami', 12.99);
    component.addPizza(p1);
    component.addPizza(p1);
    fixture.detectChanges();
    const salamiCartId: number = component.cartItems.find(i => i.pizza.name === 'Salami').cartId;
    fixture.nativeElement.querySelector(`#cart-${salamiCartId}`).dispatchEvent(new Event('click'));
    fixture.detectChanges();

    expect(component.cartItems.length).toBe(2);
    expect(component.cartItems.find(i => i.pizza.name === 'Salami')).toBeTruthy();
    expect(component.cartItems.find(i => i.pizza.name === 'Salami').count).toBe(2);


  }));


  function printPizzaCartForDebug(cart: PizzaCartItem[], debugInfo?: string) {
    if (debugInfo !== undefined) {
      console.log(debugInfo);
    }
    component.cartItems.map(i => `cartid:${i.cartId} pizzaName:${i.pizza.name} count:${i.count}`).forEach(i => console.log(i));


  }




});
