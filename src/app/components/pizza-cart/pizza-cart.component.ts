import { Pizza } from '../../models/pizza';
import { PizzaCartItem } from '../../models/pizza-cart-item';


import { Component, EventEmitter, OnInit } from '@angular/core';
import { log } from 'util';

@Component({
  selector: 'app-pizza-cart',
  templateUrl: './pizza-cart.component.html',
  styleUrls: ['./pizza-cart.component.css']
})
export class PizzaCartComponent implements OnInit {
  private _cartItems: PizzaCartItem[] = [];
  private cartMessage: string;

  constructor() { }

  ngOnInit() {
  }

  removePizzaCartItem(cartId: number) {
    const itemToRemoveOrDecrement: PizzaCartItem = this._cartItems.find(i => i.cartId === cartId);
    this._cartItems = this._cartItems.filter(i => i.cartId !== cartId);
    if (itemToRemoveOrDecrement.count >= 2) {
      itemToRemoveOrDecrement.count--;
      this._cartItems.push(itemToRemoveOrDecrement);
    }
  }

  addPizza(p: Pizza) {
    if (this._cartItems.find(pi => pi.pizza.id === p.id)) {
      const item: PizzaCartItem = this._cartItems.find(pi => pi.pizza.id === p.id);
      this._cartItems = this._cartItems.filter(pi => pi.pizza.id !== p.id);
      if (item.pizza.name !== 'Flyer') {
        item.count++;
      } else {
        this.cartMessage = 'Nur ein Flyer pro Bestellung';
      }
      this._cartItems.push(item);
    } else {
      const item: PizzaCartItem = new PizzaCartItem(p, this._cartItems.length + 1);
      this._cartItems.push(item);
    }
  }

  get cartItems(): PizzaCartItem[] {
    return this._cartItems.sort((i1, i2) => i1.compare(i2));
  }

  get cartsum(): number {
    return this._cartItems.map(i => i.pizza.price * i.count).reduce(((itemPrice, sum) => itemPrice + sum), 0);
  }
}
