import { Component, Injectable, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.css']
})

@Injectable()
export class PriceComponent implements OnInit {


  @Input() value: number;

  constructor() { }

  ngOnInit() {
  }

  get rounded(): number {
    return Math.round(this.value * 100) / 100;
  }

}
