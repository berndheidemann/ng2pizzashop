import { Component, OnInit, Input } from '@angular/core';
import { PizzaCartItem } from '../../models/pizza-cart-item';
import { PriceComponent } from '../price/price.component';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.css']
})
export class CartItemComponent implements OnInit {


  @Input() item: PizzaCartItem;

  constructor() { }

  ngOnInit() {
  }

}
