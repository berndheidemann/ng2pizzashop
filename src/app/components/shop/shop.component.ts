import { Pizza } from '../../models/pizza';
import { PizzaCartItem } from '../../models/pizza-cart-item';
import { PizzaDataServiceService } from '../../services/pizza-data-service.service';
import { Component, Output, ViewChild, EventEmitter } from '@angular/core';
import { PriceComponent } from '../../components/price/price.component';
import { PizzaCartComponent } from '../../components/pizza-cart/pizza-cart.component';

@Component({
  selector: 'app-root',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})

export class ShopComponent {

  private cart: PizzaCartItem[] = [];

  private _detailPizza: Pizza = new Pizza(-1, 'foo', 0);
  @ViewChild(PizzaCartComponent)
  private pizzaCartComponent: PizzaCartComponent;

  @ViewChild('pizzaDetailModal') pizzaDetailModal;


  constructor(private pizzaDataService: PizzaDataServiceService) {
    
  }

  get detailPizza(): Pizza {
    return this._detailPizza;
  }

  get pizzas(): Pizza[] {
    return this.pizzaDataService.pizzas;
  }

  addToCart(pizza: Pizza) {
    this.pizzaCartComponent.addPizza(pizza);
  }

  showPizzaDetails(pizza: Pizza) {
    this._detailPizza = pizza;
    this.pizzaDetailModal.show();
  }




}
