/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { AppModule } from '../../app.module';
import { CartItemComponent } from '../../components/cart-item/cart-item.component';
import { PizzaCartComponent } from '../../components/pizza-cart/pizza-cart.component';
import { PriceComponent } from '../../components/price/price.component';
import { Pizza } from '../../models/pizza';
import { PizzaPipe } from '../../pipes/pizza.pipe';
import { PizzaDataServiceMock } from '../../services/pizza-data-service-mock';
import { PizzaDataServiceService } from '../../services/pizza-data-service.service';
import { FormsModule } from '@angular/forms';
import { ShopComponent } from './shop.component';
import { AlertModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';

describe('ShopComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ModalModule.forRoot()],
      declarations: [
        ShopComponent, PizzaPipe, PriceComponent, CartItemComponent, PizzaCartComponent
      ]
    });
    TestBed.overrideComponent(ShopComponent, {
      set: {
        providers: [
          { provide: PizzaDataServiceService, useClass: PizzaDataServiceMock },
        ]
      }
    });
    TestBed.compileComponents();
  });

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(ShopComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  /*it(`should have as title 'app works!'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app works!');
  }));
*/
  it('should display Pizzas by SearchField', async(() => {
    const fixture = TestBed.createComponent(ShopComponent);
    fixture.detectChanges();
    fixture.nativeElement.querySelector('input#suchfeld').value = 'Rind';
    fixture.nativeElement.querySelector('input#suchfeld').dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('tr.pizza-2 td').textContent).toEqual('2');
  }));

  it('should add Pizza to Cart on click', async(() => {
    const fixture = TestBed.createComponent(ShopComponent);
    fixture.detectChanges();
    fixture.nativeElement.querySelector('button#pizza-2').dispatchEvent(new Event('click'));
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#cart-1').textContent).toContain('Rinderfilet mit Blattgold');
  }));

  it('should display CartSum', async(() => {
    const fixture = TestBed.createComponent(ShopComponent);
    fixture.detectChanges();
    fixture.nativeElement.querySelector('button#pizza-2').dispatchEvent(new Event('click'));
    fixture.nativeElement.querySelector('button#pizza-1').dispatchEvent(new Event('click'));

    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#cartsum').textContent).toContain('51.98');
  }));


  it('should return all Pizzas from the DataService', async(() => {
    const fixture = TestBed.createComponent(ShopComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.pizzas[0].name === 'Käse Salami Kobe').toBeTruthy();
  }));

  it('should use the PizzaPipe', async(() => {
    const fixture = TestBed.createComponent(ShopComponent);
    const app = fixture.debugElement.componentInstance;
    const pipe: PizzaPipe = new PizzaPipe();
    const filtered: Pizza[] = pipe.transform(app.pizzas, 'Käse');
    expect(filtered[0].name === 'Käse Salami Kobe').toBeTruthy();
  }));

  it('products with price 0 should display "kostenlos"', async(() => {
    const fixture = TestBed.createComponent(ShopComponent);
    fixture.detectChanges();
    fixture.nativeElement.querySelector('button#pizza-3').dispatchEvent(new Event('click'));
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('#cart-1 .price').textContent).toContain('kostenlos');
  }));

  it('should save the pizza for DetailModal', async(() => {
    const fixture = TestBed.createComponent(ShopComponent);
    fixture.detectChanges();
    fixture.nativeElement.querySelector('.pizza-2').dispatchEvent(new Event('click'));
    const app: ShopComponent = fixture.debugElement.componentInstance;
    fixture.detectChanges();
    expect(app.detailPizza.name).toBe('Rinderfilet mit Blattgold');
  }));

 



  /*it('should save a Pizza to the DataService', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.pizzas[0].name === 'Käse Salami').toBeTruthy();
  }));*/
});
