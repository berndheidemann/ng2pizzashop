import { Component, OnInit } from '@angular/core';
import { Pizza } from '../../models/pizza';
import { PizzaDataServiceService } from '../../services/pizza-data-service.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-add-pizza',
  templateUrl: './add-pizza.component.html',
  styleUrls: ['./add-pizza.component.css']
})
export class AddPizzaComponent implements OnInit {
  constructor(public pizzaDataService: PizzaDataServiceService, private router: Router) { 
  }

  ngOnInit() {
  }

  savePizza(pizza: Pizza) {
    this.pizzaDataService.addPizza(new Pizza(pizza.id, pizza.name, pizza.price));
    // funktioniert, aber offenbart eine Testhölle.... Router lässt sich atm nicht mocken, weiter recherchieren
    this.router.navigateByUrl('/shop');

  }

}
