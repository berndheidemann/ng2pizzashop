/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { PizzaDataServiceMock } from '../../services/pizza-data-service-mock';
import { PizzaDataServiceService } from '../../services/pizza-data-service.service';
import { Pizza } from '../../models/pizza';
import { AddPizzaComponent } from './add-pizza.component';
import { log } from 'util';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';


class RouterStub {
  navigateByUrl(url: string) {
    return url;
  }

  navigate(url: any) {
    return url;
  }
}


describe('AddPizzaComponent', () => {
  let component: AddPizzaComponent;
  let fixture: ComponentFixture<AddPizzaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddPizzaComponent],
      imports: [FormsModule],
      providers: [{ provide: Router, useClass: RouterStub }]

    })
      .compileComponents();
    TestBed.overrideComponent(AddPizzaComponent, {
      set: {
        providers: [
          { provide: PizzaDataServiceService, useClass: PizzaDataServiceMock },
        ]
      }
    });
    TestBed.compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPizzaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should save a Pizza to the pizzaDataService', () => {
    const count: number = component.pizzaDataService.pizzas.length;
    component.savePizza(new Pizza(1337, 'Lauch Platin', 99.99));
    expect(component.pizzaDataService.getPizzaById(1337).name).toBe('Lauch Platin');
    expect(component.pizzaDataService.pizzas.length).toBe(count + 1);

  });



});
