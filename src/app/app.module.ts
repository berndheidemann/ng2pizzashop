import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { PizzaPipe } from './pipes/pizza.pipe';
import { PriceComponent } from './components/price/price.component';
import { CartItemComponent } from './components/cart-item/cart-item.component';
import { PizzaCartComponent } from './components/pizza-cart/pizza-cart.component';
import { ImpressumComponent } from './components/impressum/impressum.component';
import { AnfahrtComponent } from './components/anfahrt/anfahrt.component';
import { ShopComponent } from './components/shop/shop.component';
import { RouterModule } from '@angular/router';
import { routerConfig } from './routing/routingConfig';
import { AlertModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AddPizzaComponent } from './components/add-pizza/add-pizza.component';
import { PizzaDataServiceService } from './services/pizza-data-service.service';

@NgModule({
  declarations: [
    AppComponent,
    PizzaPipe,
    PriceComponent,
    CartItemComponent,
    PizzaCartComponent,
    ImpressumComponent,
    AnfahrtComponent,
    ShopComponent,
    AddPizzaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routerConfig),
    HttpModule,
    ModalModule.forRoot()
  ],
  exports: [
    RouterModule
  ],
  providers: [PizzaDataServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }


